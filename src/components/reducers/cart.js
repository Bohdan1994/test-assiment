import {ADD_QUANTITY, ADD_TO_CART, REMOVE_ITEM, SUB_QUANTITY} from "../actions/action-types/cart-actions";
import {initialState} from "./initialState";
import {getItem} from "../../utills";

export default (state = [], action) => {
  let cart;

  switch (action.type) {
    case ADD_TO_CART:
      cart = getItem(initialState, action.id);
      return [...new Set([...state, cart])];

    case ADD_QUANTITY:
      cart = getItem(initialState, action.id);
      cart.count += 1;
      return [...new Set([...state, cart])];

    case SUB_QUANTITY:
      cart = getItem(initialState, action.id);
      cart.count = cart.count > 1 ? cart.count - 1 : cart.count;
      return [...new Set([...state, cart])];

    case REMOVE_ITEM:
      let newState = state.filter(item => item.id !== action.id ? true : false);
      return [...new Set(newState)];

    default:
      return state;
  }
};
