import React, {useState} from "react";
import {connect} from "react-redux";
import {addToCart} from "./actions/cartActions";
import ItemCard from "./ItemCard";
import {Alert, Container, Snackbar} from "@mui/material";

export const Home = ({products, addToCart}) => {
  const [state, setState] = useState(false);
  const handleCloseAlert = () => {
    setState(false);
  };

  return (
      <Container maxWidth={'xl'}>
        <h3 className="center">Our items</h3>
        <div className="box">{products.map(product => {
          return <ItemCard
              img={product.img}
              id={product.id}
              title={product.title}
              desc={product.desc}
              price={product.price}
              addToCart={(id) => {
                addToCart(id);
                setState(true)
              }}
              key={product.title}
          />
        })}
        </div>
        <Snackbar open={state} autoHideDuration={3000} onClose={handleCloseAlert}>
          <Alert onClose={handleCloseAlert} variant={'filled'} className={'alert'}>This item has been added to your
            bucket!</Alert>
        </Snackbar>
      </Container>
  );
};

const mapStateToProps = (state) => ({
  products: state.products,
});

const mapDispatchToProps = (dispatch) => ({
  addToCart: (id) => {
    dispatch(addToCart(id));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);
