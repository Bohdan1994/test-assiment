import React from "react";
import { connect } from "react-redux";
import {
  addQuantity,
  removeItem,
  subtractQuantity,
} from "./actions/cartActions";
import ItemCard from "./ItemCard";
import {Container} from "@mui/material";

export const Cart = ({
  cartProducts,
  removeItem,
  addQuantity,
  subtractQuantity,
}) => {
  const handleRemove = (id) => {
    removeItem(id);
  };

  const handleAddQuantity = (id) => {
    addQuantity(id);
  };

  const handleSubtractQuantity = (id) => {
    subtractQuantity(id);
  };

  return (
      <Container maxWidth={'xl'}>
        <h5>{cartProducts.length
            ?'You have ordered:'
            :'Your backet is empty'
            }
        </h5>
        <div className="box">
          {
            cartProducts.map((product) => {
              return <ItemCard
                  img={product.img}
                  count={product.count}
                  id={product.id}
                  title={product.title}
                  desc={product.desc}
                  price={product.price}
                  addToCart={handleAddQuantity}
                  subtractQuantity={handleSubtractQuantity}
                  removeFromCard={handleRemove}
                  key={product.title}
              />
            })
          }
        </div>
      </Container>
  );
};

const mapStateToProps = (state) => {
  return {
    cartProducts: state.cart,
  };
};

const mapDispatchToProps = (dispatch) => ({
  removeItem: (id) => {
    dispatch(removeItem(id));
  },
  addQuantity: (id) => {
    dispatch(addQuantity(id));
  },
  subtractQuantity: (id) => {
    dispatch(subtractQuantity(id));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(Cart);
