import React from "react";
import { Link } from "react-router-dom";
import {connect} from "react-redux";
import {Container} from "@mui/material";

const Navbar = ({cartProducts}) => {
  const count = cartProducts.reduce((acc, next) => acc += next.count, 0);

  return <nav className="nav-wrapper">
    <Container maxWidth={'xl'}>
      <Link to="/" className="brand-logo">Shopping</Link>
      <ul className="right">
        <li><Link to="/">Shop</Link></li>
        <li><Link to="/cart">My cart</Link></li>
        <li className={'bucket-icon'}>
          <Link to="/cart">
            <span className={'bucket_counter'}>{count}</span>
            <i className="material-icons">shopping_cart</i></Link>
        </li>
      </ul>
    </Container>
  </nav>
};

const mapStateToProps = (state) => {
  return {
    cartProducts: state.cart,
  };
};

export default connect(mapStateToProps)(Navbar);
