import React from 'react';

const Add = ({onClick}) => {
  return (
      <span
          to="/"
          className="btn-floating halfway-fab waves-effect waves-light red"
          onClick={onClick}
          style={{right: 1 + 'rem'}}
      >
        <i className="material-icons">add</i>
      </span>
  )
}

const EditCount = ({onClick, disabled}) => {
  return (
      <span
          to="/"
          className={`btn-floating halfway-fab waves-effect waves-light red ${disabled ? 'disabled' : ''}`}
          onClick={onClick}
          style={{right: 'calc(1.5rem + 40px)'}}
      >
        <i className="material-icons">remove</i>
      </span>
  )
};

const RemoveItem = ({onClick}) => {
  return ( <span
          to="/"
          className={`btn-floating halfway-fab waves-effect waves-light blue`}
          onClick={onClick}
          style={{right: 1 + 'rem', top: '1rem'}}
      >
        <i className="material-icons">delete</i>
      </span>)
}

const ItemCard = ({img, title, addToCart, removeFromCard, desc, price, id, count, subtractQuantity}) => {
  const priceTitle = count ? price * count : price;

  return <div className="card">
    <div className="card-image">
      <img src={img} alt={title}/>
      <span className="card-title">{title}</span>
      <Add onClick={() => addToCart(id)}/>
      {
        subtractQuantity ? <EditCount onClick={() => subtractQuantity(id)} disabled={count <= 1}/> : null
      }
      {
        removeFromCard ? <RemoveItem onClick={() => removeFromCard(id) } />:null
      }

    </div>

    <div className="card-content">
      <p>{desc}</p>
      <p>
        <b>
          Price:
          {priceTitle}$
        </b>
        {
          count ? <b className={'card-content__count'}>Count:{count}</b> : null
        }
      </p>
    </div>
  </div>
};

export default ItemCard;